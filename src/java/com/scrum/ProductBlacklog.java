/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scrum;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Ezequiel Bertone
 */
@Entity
@Table(name = "product_blacklog")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProductBlacklog.findAll", query = "SELECT p FROM ProductBlacklog p")
    , @NamedQuery(name = "ProductBlacklog.findByIdProduct", query = "SELECT p FROM ProductBlacklog p WHERE p.idProduct = :idProduct")
    , @NamedQuery(name = "ProductBlacklog.findByPrioridad", query = "SELECT p FROM ProductBlacklog p WHERE p.prioridad = :prioridad")
    , @NamedQuery(name = "ProductBlacklog.findByNroHistoriaUsuario", query = "SELECT p FROM ProductBlacklog p WHERE p.nroHistoriaUsuario = :nroHistoriaUsuario")
    , @NamedQuery(name = "ProductBlacklog.findByHistoriaUsuario", query = "SELECT p FROM ProductBlacklog p WHERE p.historiaUsuario = :historiaUsuario")
    , @NamedQuery(name = "ProductBlacklog.findByHorasEstimadas", query = "SELECT p FROM ProductBlacklog p WHERE p.horasEstimadas = :horasEstimadas")})
public class ProductBlacklog implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_product")
    private Integer idProduct;
    @Column(name = "prioridad")
    private Integer prioridad;
    @Column(name = "nro_historia_usuario")
    private Integer nroHistoriaUsuario;
    @Size(max = 500)
    @Column(name = "historia_usuario")
    private String historiaUsuario;
    @Column(name = "horas_estimadas")
    private Integer horasEstimadas;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idProduct")
    private Collection<Sprint> sprintCollection;
    @JoinColumn(name = "id_proyecto", referencedColumnName = "id_proyecto")
    @ManyToOne(optional = false)
    private Proyecto idProyecto;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idProduct")
    private Collection<UserHistories> userHistoriesCollection;

    public ProductBlacklog() {
    }

    public ProductBlacklog(Integer idProduct) {
        this.idProduct = idProduct;
    }

    public Integer getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(Integer idProduct) {
        this.idProduct = idProduct;
    }

    public Integer getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(Integer prioridad) {
        this.prioridad = prioridad;
    }

    public Integer getNroHistoriaUsuario() {
        return nroHistoriaUsuario;
    }

    public void setNroHistoriaUsuario(Integer nroHistoriaUsuario) {
        this.nroHistoriaUsuario = nroHistoriaUsuario;
    }

    public String getHistoriaUsuario() {
        return historiaUsuario;
    }

    public void setHistoriaUsuario(String historiaUsuario) {
        this.historiaUsuario = historiaUsuario;
    }

    public Integer getHorasEstimadas() {
        return horasEstimadas;
    }

    public void setHorasEstimadas(Integer horasEstimadas) {
        this.horasEstimadas = horasEstimadas;
    }

    @XmlTransient
    public Collection<Sprint> getSprintCollection() {
        return sprintCollection;
    }

    public void setSprintCollection(Collection<Sprint> sprintCollection) {
        this.sprintCollection = sprintCollection;
    }

    public Proyecto getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(Proyecto idProyecto) {
        this.idProyecto = idProyecto;
    }

    @XmlTransient
    public Collection<UserHistories> getUserHistoriesCollection() {
        return userHistoriesCollection;
    }

    public void setUserHistoriesCollection(Collection<UserHistories> userHistoriesCollection) {
        this.userHistoriesCollection = userHistoriesCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProduct != null ? idProduct.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductBlacklog)) {
            return false;
        }
        ProductBlacklog other = (ProductBlacklog) object;
        if ((this.idProduct == null && other.idProduct != null) || (this.idProduct != null && !this.idProduct.equals(other.idProduct))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.scrum.ProductBlacklog[ idProduct=" + idProduct + " ]";
    }
    
}
