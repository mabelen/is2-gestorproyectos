/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scrum;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Ezequiel Bertone
 */
@Entity
@Table(name = "sprint")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Sprint.findAll", query = "SELECT s FROM Sprint s")
    , @NamedQuery(name = "Sprint.findByIdSprint", query = "SELECT s FROM Sprint s WHERE s.idSprint = :idSprint")
    , @NamedQuery(name = "Sprint.findByNombre", query = "SELECT s FROM Sprint s WHERE s.nombre = :nombre")
    , @NamedQuery(name = "Sprint.findByEstado", query = "SELECT s FROM Sprint s WHERE s.estado = :estado")
    , @NamedQuery(name = "Sprint.findByUsuarioAsignados", query = "SELECT s FROM Sprint s WHERE s.usuarioAsignados = :usuarioAsignados")
    , @NamedQuery(name = "Sprint.findByPrioridad", query = "SELECT s FROM Sprint s WHERE s.prioridad = :prioridad")
    , @NamedQuery(name = "Sprint.findByHistoriaUsuario", query = "SELECT s FROM Sprint s WHERE s.historiaUsuario = :historiaUsuario")
    , @NamedQuery(name = "Sprint.findByHorasEstimadas", query = "SELECT s FROM Sprint s WHERE s.horasEstimadas = :horasEstimadas")
    , @NamedQuery(name = "Sprint.findByNroHistoriaUsuario", query = "SELECT s FROM Sprint s WHERE s.nroHistoriaUsuario = :nroHistoriaUsuario")})
public class Sprint implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_sprint")
    private Integer idSprint;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "estado")
    private String estado;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "usuario_asignados")
    private String usuarioAsignados;
    @Column(name = "prioridad")
    private Integer prioridad;
    @Size(max = 500)
    @Column(name = "historia_usuario")
    private String historiaUsuario;
    @Column(name = "horas_estimadas")
    private Integer horasEstimadas;
    @Column(name = "nro_historia_usuario")
    private Integer nroHistoriaUsuario;
    @JoinColumn(name = "id_product", referencedColumnName = "id_product")
    @ManyToOne(optional = false)
    private ProductBlacklog idProduct;

    public Sprint() {
    }

    public Sprint(Integer idSprint) {
        this.idSprint = idSprint;
    }

    public Sprint(Integer idSprint, String nombre, String estado, String usuarioAsignados) {
        this.idSprint = idSprint;
        this.nombre = nombre;
        this.estado = estado;
        this.usuarioAsignados = usuarioAsignados;
    }

    public Integer getIdSprint() {
        return idSprint;
    }

    public void setIdSprint(Integer idSprint) {
        this.idSprint = idSprint;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getUsuarioAsignados() {
        return usuarioAsignados;
    }

    public void setUsuarioAsignados(String usuarioAsignados) {
        this.usuarioAsignados = usuarioAsignados;
    }

    public Integer getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(Integer prioridad) {
        this.prioridad = prioridad;
    }

    public String getHistoriaUsuario() {
        return historiaUsuario;
    }

    public void setHistoriaUsuario(String historiaUsuario) {
        this.historiaUsuario = historiaUsuario;
    }

    public Integer getHorasEstimadas() {
        return horasEstimadas;
    }

    public void setHorasEstimadas(Integer horasEstimadas) {
        this.horasEstimadas = horasEstimadas;
    }

    public Integer getNroHistoriaUsuario() {
        return nroHistoriaUsuario;
    }

    public void setNroHistoriaUsuario(Integer nroHistoriaUsuario) {
        this.nroHistoriaUsuario = nroHistoriaUsuario;
    }

    public ProductBlacklog getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(ProductBlacklog idProduct) {
        this.idProduct = idProduct;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSprint != null ? idSprint.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sprint)) {
            return false;
        }
        Sprint other = (Sprint) object;
        if ((this.idSprint == null && other.idSprint != null) || (this.idSprint != null && !this.idSprint.equals(other.idSprint))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.scrum.Sprint[ idSprint=" + idSprint + " ]";
    }
    
}
