package com.scrum.core;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.RequestScoped;
import org.primefaces.context.RequestContext;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;


@ManagedBean(name = "login")
@RequestScoped
public class LoginBean {

    private String usuario;
    private String clave;

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

   
    
    
    /**
     * Creates a new instance of LoginBean
     */
    public LoginBean() {
    }
    
    public void login(){
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("swal('Login correcto','Se ha logeado','success')");
        
    }
    
    public String loginProject() {
    boolean result = LoginDAO.login(usuario, clave);
        if (result) {
            System.out.println("Entro en redireccion");
            HttpSession session = Util.getSession();
            session.setAttribute("username", usuario);            
            
            
            FacesContext ctxt = FacesContext.getCurrentInstance();
            ctxt.getExternalContext().getFlash().setKeepMessages(true);
            try {
                ctxt.getExternalContext().redirect("index.xhtml");
            } catch (IOException ex) {
                Logger.getLogger(LoginBean.class.getName()).log(Level.SEVERE, null, ex);
            }
            return "index";
            
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
                    "Invalid Login!",
                    "Please Try Again!"));
            
             // invalidate session, and redirect to other pages
             //message = "Invalid Login. Please Try Again!";
            return "index";
        }
    }
 
    public String logout() {
      HttpSession session = Util.getSession();
      session.invalidate();
      return "login";
   }
    
    
    
}
