package com.scrum.core;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBCon {
 
    public Connection getConnection() {

        Connection con = null;

	try {
            String dbDriver = "org.postgresql.Driver";
            String dbURL = "jdbc:postgresql://localhost:5432/scrum";
            Class.forName(dbDriver);
            con = DriverManager.getConnection(dbURL,"postgres","12345678");
            
            System.out.println("DB Connecting");
	} catch (ClassNotFoundException | SQLException e) {
	     e.printStackTrace();
	     System.out.println("Database.getConnection() Error -->" + e.getMessage());
	}
	return con;
	}
        
    public void close(Connection con) {
        try {
            con.close();
        } catch (Exception ex) {
        }
    }
    
    
    
    
}
