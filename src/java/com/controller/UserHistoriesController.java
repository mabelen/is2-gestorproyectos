package com.controller;

import com.scrum.UserHistories;
import com.facade.UserHistoriesFacade;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

@Named(value = "userHistoriesController")
@ViewScoped
public class UserHistoriesController extends AbstractController<UserHistories> {

    @Inject
    private ProductBlacklogController idProductController;

    public UserHistoriesController() {
        // Inform the Abstract parent controller of the concrete UserHistories Entity
        super(UserHistories.class);
    }

    /**
     * Resets the "selected" attribute of any parent Entity controllers.
     */
    public void resetParents() {
        idProductController.setSelected(null);
    }

    /**
     * Sets the "selected" attribute of the ProductBlacklog controller in order
     * to display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareIdProduct(ActionEvent event) {
        UserHistories selected = this.getSelected();
        if (selected != null && idProductController.getSelected() == null) {
            idProductController.setSelected(selected.getIdProduct());
        }
    }

}
