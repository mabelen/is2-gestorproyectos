package com.controller;

import com.scrum.Rol;
import com.scrum.Asignar;
import java.util.Collection;
import com.facade.RolFacade;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

@Named(value = "rolController")
@ViewScoped
public class RolController extends AbstractController<Rol> {

    // Flags to indicate if child collections are empty
    private boolean isAsignarCollectionEmpty;

    public RolController() {
        // Inform the Abstract parent controller of the concrete Rol Entity
        super(Rol.class);
    }

    /**
     * Set the "is[ChildCollection]Empty" property for OneToMany fields.
     */
    @Override
    protected void setChildrenEmptyFlags() {
        this.setIsAsignarCollectionEmpty();
    }

    public boolean getIsAsignarCollectionEmpty() {
        return this.isAsignarCollectionEmpty;
    }

    private void setIsAsignarCollectionEmpty() {
        Rol selected = this.getSelected();
        if (selected != null) {
            RolFacade ejbFacade = (RolFacade) this.getFacade();
            this.isAsignarCollectionEmpty = ejbFacade.isAsignarCollectionEmpty(selected);
        } else {
            this.isAsignarCollectionEmpty = true;
        }
    }

    /**
     * Sets the "items" attribute with a collection of Asignar entities that are
     * retrieved from Rol and returns the navigation outcome.
     *
     * @return navigation outcome for Asignar page
     */
    public String navigateAsignarCollection() {
        Rol selected = this.getSelected();
        if (selected != null) {
            RolFacade ejbFacade = (RolFacade) this.getFacade();
            Collection<Asignar> selectedAsignarCollection = ejbFacade.findAsignarCollection(selected);
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Asignar_items", selectedAsignarCollection);
        }
        return "/app/asignar/index";
    }

}
