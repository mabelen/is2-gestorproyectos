package com.controller;

import com.scrum.ProductBlacklog;
import com.scrum.Sprint;
import com.scrum.UserHistories;
import java.util.Collection;
import com.facade.ProductBlacklogFacade;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

@Named(value = "productBlacklogController")
@ViewScoped
public class ProductBlacklogController extends AbstractController<ProductBlacklog> {

    @Inject
    private ProyectoController idProyectoController;

    // Flags to indicate if child collections are empty
    private boolean isSprintCollectionEmpty;
    private boolean isUserHistoriesCollectionEmpty;

    public ProductBlacklogController() {
        // Inform the Abstract parent controller of the concrete ProductBlacklog Entity
        super(ProductBlacklog.class);
    }

    /**
     * Resets the "selected" attribute of any parent Entity controllers.
     */
    public void resetParents() {
        idProyectoController.setSelected(null);
    }

    /**
     * Set the "is[ChildCollection]Empty" property for OneToMany fields.
     */
    @Override
    protected void setChildrenEmptyFlags() {
        this.setIsSprintCollectionEmpty();
        this.setIsUserHistoriesCollectionEmpty();
    }

    public boolean getIsSprintCollectionEmpty() {
        return this.isSprintCollectionEmpty;
    }

    private void setIsSprintCollectionEmpty() {
        ProductBlacklog selected = this.getSelected();
        if (selected != null) {
            ProductBlacklogFacade ejbFacade = (ProductBlacklogFacade) this.getFacade();
            this.isSprintCollectionEmpty = ejbFacade.isSprintCollectionEmpty(selected);
        } else {
            this.isSprintCollectionEmpty = true;
        }
    }

    /**
     * Sets the "items" attribute with a collection of Sprint entities that are
     * retrieved from ProductBlacklog and returns the navigation outcome.
     *
     * @return navigation outcome for Sprint page
     */
    public String navigateSprintCollection() {
        ProductBlacklog selected = this.getSelected();
        if (selected != null) {
            ProductBlacklogFacade ejbFacade = (ProductBlacklogFacade) this.getFacade();
            Collection<Sprint> selectedSprintCollection = ejbFacade.findSprintCollection(selected);
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Sprint_items", selectedSprintCollection);
        }
        return "/app/sprint/index";
    }

    /**
     * Sets the "selected" attribute of the Proyecto controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareIdProyecto(ActionEvent event) {
        ProductBlacklog selected = this.getSelected();
        if (selected != null && idProyectoController.getSelected() == null) {
            idProyectoController.setSelected(selected.getIdProyecto());
        }
    }

    public boolean getIsUserHistoriesCollectionEmpty() {
        return this.isUserHistoriesCollectionEmpty;
    }

    private void setIsUserHistoriesCollectionEmpty() {
        ProductBlacklog selected = this.getSelected();
        if (selected != null) {
            ProductBlacklogFacade ejbFacade = (ProductBlacklogFacade) this.getFacade();
            this.isUserHistoriesCollectionEmpty = ejbFacade.isUserHistoriesCollectionEmpty(selected);
        } else {
            this.isUserHistoriesCollectionEmpty = true;
        }
    }

    /**
     * Sets the "items" attribute with a collection of UserHistories entities
     * that are retrieved from ProductBlacklog and returns the navigation
     * outcome.
     *
     * @return navigation outcome for UserHistories page
     */
    public String navigateUserHistoriesCollection() {
        ProductBlacklog selected = this.getSelected();
        if (selected != null) {
            ProductBlacklogFacade ejbFacade = (ProductBlacklogFacade) this.getFacade();
            Collection<UserHistories> selectedUserHistoriesCollection = ejbFacade.findUserHistoriesCollection(selected);
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("UserHistories_items", selectedUserHistoriesCollection);
        }
        return "/app/userHistories/index";
    }

}
