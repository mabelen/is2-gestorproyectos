package com.controller;

import com.scrum.Proyecto;
import com.scrum.ProductBlacklog;
import com.scrum.Asignar;
import java.util.Collection;
import com.facade.ProyectoFacade;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

@Named(value = "proyectoController")
@ViewScoped
public class ProyectoController extends AbstractController<Proyecto> {

    // Flags to indicate if child collections are empty
    private boolean isProductBlacklogCollectionEmpty;
    private boolean isAsignarCollectionEmpty;

    public ProyectoController() {
        // Inform the Abstract parent controller of the concrete Proyecto Entity
        super(Proyecto.class);
    }

    /**
     * Set the "is[ChildCollection]Empty" property for OneToMany fields.
     */
    @Override
    protected void setChildrenEmptyFlags() {
        this.setIsProductBlacklogCollectionEmpty();
        this.setIsAsignarCollectionEmpty();
    }

    public boolean getIsProductBlacklogCollectionEmpty() {
        return this.isProductBlacklogCollectionEmpty;
    }

    private void setIsProductBlacklogCollectionEmpty() {
        Proyecto selected = this.getSelected();
        if (selected != null) {
            ProyectoFacade ejbFacade = (ProyectoFacade) this.getFacade();
            this.isProductBlacklogCollectionEmpty = ejbFacade.isProductBlacklogCollectionEmpty(selected);
        } else {
            this.isProductBlacklogCollectionEmpty = true;
        }
    }

    /**
     * Sets the "items" attribute with a collection of ProductBlacklog entities
     * that are retrieved from Proyecto and returns the navigation outcome.
     *
     * @return navigation outcome for ProductBlacklog page
     */
    public String navigateProductBlacklogCollection() {
        Proyecto selected = this.getSelected();
        if (selected != null) {
            ProyectoFacade ejbFacade = (ProyectoFacade) this.getFacade();
            Collection<ProductBlacklog> selectedProductBlacklogCollection = ejbFacade.findProductBlacklogCollection(selected);
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("ProductBlacklog_items", selectedProductBlacklogCollection);
        }
        return "/app/productBlacklog/index";
    }

    public boolean getIsAsignarCollectionEmpty() {
        return this.isAsignarCollectionEmpty;
    }

    private void setIsAsignarCollectionEmpty() {
        Proyecto selected = this.getSelected();
        if (selected != null) {
            ProyectoFacade ejbFacade = (ProyectoFacade) this.getFacade();
            this.isAsignarCollectionEmpty = ejbFacade.isAsignarCollectionEmpty(selected);
        } else {
            this.isAsignarCollectionEmpty = true;
        }
    }

    /**
     * Sets the "items" attribute with a collection of Asignar entities that are
     * retrieved from Proyecto and returns the navigation outcome.
     *
     * @return navigation outcome for Asignar page
     */
    public String navigateAsignarCollection() {
        Proyecto selected = this.getSelected();
        if (selected != null) {
            ProyectoFacade ejbFacade = (ProyectoFacade) this.getFacade();
            Collection<Asignar> selectedAsignarCollection = ejbFacade.findAsignarCollection(selected);
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Asignar_items", selectedAsignarCollection);
        }
        return "/app/asignar/index";
    }

}
