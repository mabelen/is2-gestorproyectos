package com.controller;

import com.scrum.Asignar;
import com.facade.AsignarFacade;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

@Named(value = "asignarController")
@ViewScoped
public class AsignarController extends AbstractController<Asignar> {

    @Inject
    private ProyectoController idProyectoController;
    @Inject
    private RolController idRolController;
    @Inject
    private UsuarioController idUsarioController;

    public AsignarController() {
        // Inform the Abstract parent controller of the concrete Asignar Entity
        super(Asignar.class);
    }

    /**
     * Resets the "selected" attribute of any parent Entity controllers.
     */
    public void resetParents() {
        idProyectoController.setSelected(null);
        idRolController.setSelected(null);
        idUsarioController.setSelected(null);
    }

    /**
     * Sets the "selected" attribute of the Proyecto controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareIdProyecto(ActionEvent event) {
        Asignar selected = this.getSelected();
        if (selected != null && idProyectoController.getSelected() == null) {
            idProyectoController.setSelected(selected.getIdProyecto());
        }
    }

    /**
     * Sets the "selected" attribute of the Rol controller in order to display
     * its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareIdRol(ActionEvent event) {
        Asignar selected = this.getSelected();
        if (selected != null && idRolController.getSelected() == null) {
            idRolController.setSelected(selected.getIdRol());
        }
    }

    /**
     * Sets the "selected" attribute of the Usuario controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareIdUsario(ActionEvent event) {
        Asignar selected = this.getSelected();
        if (selected != null && idUsarioController.getSelected() == null) {
            idUsarioController.setSelected(selected.getIdUsario());
        }
    }

}
