package com.controller;

import com.scrum.Usuario;
import com.scrum.Asignar;
import java.util.Collection;
import com.facade.UsuarioFacade;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

@Named(value = "usuarioController")
@ViewScoped
public class UsuarioController extends AbstractController<Usuario> {

    // Flags to indicate if child collections are empty
    private boolean isAsignarCollectionEmpty;

    public UsuarioController() {
        // Inform the Abstract parent controller of the concrete Usuario Entity
        super(Usuario.class);
    }

    /**
     * Set the "is[ChildCollection]Empty" property for OneToMany fields.
     */
    @Override
    protected void setChildrenEmptyFlags() {
        this.setIsAsignarCollectionEmpty();
    }

    public boolean getIsAsignarCollectionEmpty() {
        return this.isAsignarCollectionEmpty;
    }

    private void setIsAsignarCollectionEmpty() {
        Usuario selected = this.getSelected();
        if (selected != null) {
            UsuarioFacade ejbFacade = (UsuarioFacade) this.getFacade();
            this.isAsignarCollectionEmpty = ejbFacade.isAsignarCollectionEmpty(selected);
        } else {
            this.isAsignarCollectionEmpty = true;
        }
    }

    /**
     * Sets the "items" attribute with a collection of Asignar entities that are
     * retrieved from Usuario and returns the navigation outcome.
     *
     * @return navigation outcome for Asignar page
     */
    public String navigateAsignarCollection() {
        Usuario selected = this.getSelected();
        if (selected != null) {
            UsuarioFacade ejbFacade = (UsuarioFacade) this.getFacade();
            Collection<Asignar> selectedAsignarCollection = ejbFacade.findAsignarCollection(selected);
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Asignar_items", selectedAsignarCollection);
        }
        return "/app/asignar/index";
    }

}
