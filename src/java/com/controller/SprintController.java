package com.controller;

import com.scrum.Sprint;
import com.facade.SprintFacade;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

@Named(value = "sprintController")
@ViewScoped
public class SprintController extends AbstractController<Sprint> {

    @Inject
    private ProductBlacklogController idProductController;

    public SprintController() {
        // Inform the Abstract parent controller of the concrete Sprint Entity
        super(Sprint.class);
    }

    /**
     * Resets the "selected" attribute of any parent Entity controllers.
     */
    public void resetParents() {
        idProductController.setSelected(null);
    }

    /**
     * Sets the "selected" attribute of the ProductBlacklog controller in order
     * to display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareIdProduct(ActionEvent event) {
        Sprint selected = this.getSelected();
        if (selected != null && idProductController.getSelected() == null) {
            idProductController.setSelected(selected.getIdProduct());
        }
    }

}
