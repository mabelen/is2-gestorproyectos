/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.facade;

import com.scrum.Asignar;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import com.scrum.Asignar_;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.scrum.Proyecto;
import com.scrum.Rol;
import com.scrum.Usuario;

/**
 *
 * @author Ezequiel Bertone
 */
@Stateless
public class AsignarFacade extends AbstractFacade<Asignar> {

    @PersistenceContext(unitName = "WebSCRUMPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AsignarFacade() {
        super(Asignar.class);
    }

    public boolean isIdProyectoEmpty(Asignar entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Asignar> asignar = cq.from(Asignar.class);
        cq.select(cb.literal(1L)).distinct(true).where(cb.equal(asignar, entity), cb.isNotNull(asignar.get(Asignar_.idProyecto)));
        return em.createQuery(cq).getResultList().isEmpty();
    }

    public Proyecto findIdProyecto(Asignar entity) {
        return this.getMergedEntity(entity).getIdProyecto();
    }

    public boolean isIdRolEmpty(Asignar entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Asignar> asignar = cq.from(Asignar.class);
        cq.select(cb.literal(1L)).distinct(true).where(cb.equal(asignar, entity), cb.isNotNull(asignar.get(Asignar_.idRol)));
        return em.createQuery(cq).getResultList().isEmpty();
    }

    public Rol findIdRol(Asignar entity) {
        return this.getMergedEntity(entity).getIdRol();
    }

    public boolean isIdUsarioEmpty(Asignar entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Asignar> asignar = cq.from(Asignar.class);
        cq.select(cb.literal(1L)).distinct(true).where(cb.equal(asignar, entity), cb.isNotNull(asignar.get(Asignar_.idUsario)));
        return em.createQuery(cq).getResultList().isEmpty();
    }

    public Usuario findIdUsario(Asignar entity) {
        return this.getMergedEntity(entity).getIdUsario();
    }
    
}
