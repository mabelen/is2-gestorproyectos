/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.facade;

import com.scrum.Proyecto;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import com.scrum.Proyecto_;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.scrum.ProductBlacklog;
import com.scrum.Asignar;
import java.util.Collection;

/**
 *
 * @author Ezequiel Bertone
 */
@Stateless
public class ProyectoFacade extends AbstractFacade<Proyecto> {

    @PersistenceContext(unitName = "WebSCRUMPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ProyectoFacade() {
        super(Proyecto.class);
    }

    public boolean isProductBlacklogCollectionEmpty(Proyecto entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Proyecto> proyecto = cq.from(Proyecto.class);
        cq.select(cb.literal(1L)).distinct(true).where(cb.equal(proyecto, entity), cb.isNotEmpty(proyecto.get(Proyecto_.productBlacklogCollection)));
        return em.createQuery(cq).getResultList().isEmpty();
    }

    public Collection<ProductBlacklog> findProductBlacklogCollection(Proyecto entity) {
        Proyecto mergedEntity = this.getMergedEntity(entity);
        Collection<ProductBlacklog> productBlacklogCollection = mergedEntity.getProductBlacklogCollection();
        productBlacklogCollection.size();
        return productBlacklogCollection;
    }

    public boolean isAsignarCollectionEmpty(Proyecto entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Proyecto> proyecto = cq.from(Proyecto.class);
        cq.select(cb.literal(1L)).distinct(true).where(cb.equal(proyecto, entity), cb.isNotEmpty(proyecto.get(Proyecto_.asignarCollection)));
        return em.createQuery(cq).getResultList().isEmpty();
    }

    public Collection<Asignar> findAsignarCollection(Proyecto entity) {
        Proyecto mergedEntity = this.getMergedEntity(entity);
        Collection<Asignar> asignarCollection = mergedEntity.getAsignarCollection();
        asignarCollection.size();
        return asignarCollection;
    }
    
}
