/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.facade;

import com.scrum.ProductBlacklog;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import com.scrum.ProductBlacklog_;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.scrum.Sprint;
import com.scrum.Proyecto;
import com.scrum.UserHistories;
import java.util.Collection;

/**
 *
 * @author Ezequiel Bertone
 */
@Stateless
public class ProductBlacklogFacade extends AbstractFacade<ProductBlacklog> {

    @PersistenceContext(unitName = "WebSCRUMPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ProductBlacklogFacade() {
        super(ProductBlacklog.class);
    }

    public boolean isSprintCollectionEmpty(ProductBlacklog entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<ProductBlacklog> productBlacklog = cq.from(ProductBlacklog.class);
        cq.select(cb.literal(1L)).distinct(true).where(cb.equal(productBlacklog, entity), cb.isNotEmpty(productBlacklog.get(ProductBlacklog_.sprintCollection)));
        return em.createQuery(cq).getResultList().isEmpty();
    }

    public Collection<Sprint> findSprintCollection(ProductBlacklog entity) {
        ProductBlacklog mergedEntity = this.getMergedEntity(entity);
        Collection<Sprint> sprintCollection = mergedEntity.getSprintCollection();
        sprintCollection.size();
        return sprintCollection;
    }

    public boolean isIdProyectoEmpty(ProductBlacklog entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<ProductBlacklog> productBlacklog = cq.from(ProductBlacklog.class);
        cq.select(cb.literal(1L)).distinct(true).where(cb.equal(productBlacklog, entity), cb.isNotNull(productBlacklog.get(ProductBlacklog_.idProyecto)));
        return em.createQuery(cq).getResultList().isEmpty();
    }

    public Proyecto findIdProyecto(ProductBlacklog entity) {
        return this.getMergedEntity(entity).getIdProyecto();
    }

    public boolean isUserHistoriesCollectionEmpty(ProductBlacklog entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<ProductBlacklog> productBlacklog = cq.from(ProductBlacklog.class);
        cq.select(cb.literal(1L)).distinct(true).where(cb.equal(productBlacklog, entity), cb.isNotEmpty(productBlacklog.get(ProductBlacklog_.userHistoriesCollection)));
        return em.createQuery(cq).getResultList().isEmpty();
    }

    public Collection<UserHistories> findUserHistoriesCollection(ProductBlacklog entity) {
        ProductBlacklog mergedEntity = this.getMergedEntity(entity);
        Collection<UserHistories> userHistoriesCollection = mergedEntity.getUserHistoriesCollection();
        userHistoriesCollection.size();
        return userHistoriesCollection;
    }
    
}
