/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.facade;

import com.scrum.UserHistories;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import com.scrum.UserHistories_;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.scrum.ProductBlacklog;

/**
 *
 * @author Ezequiel Bertone
 */
@Stateless
public class UserHistoriesFacade extends AbstractFacade<UserHistories> {

    @PersistenceContext(unitName = "WebSCRUMPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UserHistoriesFacade() {
        super(UserHistories.class);
    }

    public boolean isIdProductEmpty(UserHistories entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<UserHistories> userHistories = cq.from(UserHistories.class);
        cq.select(cb.literal(1L)).distinct(true).where(cb.equal(userHistories, entity), cb.isNotNull(userHistories.get(UserHistories_.idProduct)));
        return em.createQuery(cq).getResultList().isEmpty();
    }

    public ProductBlacklog findIdProduct(UserHistories entity) {
        return this.getMergedEntity(entity).getIdProduct();
    }
    
}
