/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.facade;

import com.scrum.Sprint;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import com.scrum.Sprint_;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.scrum.ProductBlacklog;

/**
 *
 * @author Ezequiel Bertone
 */
@Stateless
public class SprintFacade extends AbstractFacade<Sprint> {

    @PersistenceContext(unitName = "WebSCRUMPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public SprintFacade() {
        super(Sprint.class);
    }

    public boolean isIdProductEmpty(Sprint entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Sprint> sprint = cq.from(Sprint.class);
        cq.select(cb.literal(1L)).distinct(true).where(cb.equal(sprint, entity), cb.isNotNull(sprint.get(Sprint_.idProduct)));
        return em.createQuery(cq).getResultList().isEmpty();
    }

    public ProductBlacklog findIdProduct(Sprint entity) {
        return this.getMergedEntity(entity).getIdProduct();
    }
    
}
